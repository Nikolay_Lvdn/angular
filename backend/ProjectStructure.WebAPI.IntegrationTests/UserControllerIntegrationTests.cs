using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DataAccess;
using ProjectStructure.WebAPI.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class UserControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private CompanyDbContext _context;

        public UserControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();

            factory.DBName = "DB" + GetType().Name;

            var scopeFactory = factory.Services.GetService<IServiceScopeFactory>();
            var scope = scopeFactory.CreateScope();
            _context = scope.ServiceProvider.GetService<CompanyDbContext>();
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }

        [Fact]
        public async void CreateUserResponceWithCode201_ThanDeleteResponceWithCode204()
        {
            var httpPostResponce = await _client.PostUserAsync("Test");
            
            var stringPostResponce = await httpPostResponce.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserView>(stringPostResponce);

            Assert.Equal(HttpStatusCode.Created, httpPostResponce.StatusCode);
            Assert.Equal("Test", createdUser.FirstName);

            var httpDeleteResponce = await _client.DeleteAsync("api/User/1");

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponce.StatusCode);
        }

        [Fact]
        public async void DeleteUser_ThatDontExists_ThenDeleteResponceWithCode404()
        {
            var httpDeleteResponce = await _client.DeleteAsync("api/User/1");

            Assert.Equal(HttpStatusCode.NotFound, httpDeleteResponce.StatusCode);
        }
    }
}
