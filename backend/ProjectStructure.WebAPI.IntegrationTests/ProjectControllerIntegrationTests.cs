using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DataAccess;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private CompanyDbContext _context;

        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();

            factory.DBName = "DB" + GetType().Name;

            var scopeFactory = factory.Services.GetService<IServiceScopeFactory>();
            var scope = scopeFactory.CreateScope();
            _context = scope.ServiceProvider.GetService<CompanyDbContext>();
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }

        [Fact]
        public async void CreateProject_ThanResponceWithCode201()
        {
            var httpResponce = await _client.PostProjectAsync("Project");

            var stringResponce = await httpResponce.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectView>(stringResponce);

            Assert.Equal(HttpStatusCode.Created, httpResponce.StatusCode);
            Assert.Equal("Project", createdProject.Name);
        }

        [Fact]
        public async void Query1_NoUserWithRequestedId_ThanResponceWithCode404()
        {
            var httpResponce = await _client.GetAsync("api/Project/q1/1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponce.StatusCode);
        }

        [Fact]
        public async void Query1_GetProjectWith3TasksByUserId_ThanResponceWithCode200()
        {
            await _client.PostUserAsync("Test");

            await _client.PostProjectAsync("Test", 1);

            await _client.PostTaskAsync(name: "Test1", projectId: 1);

            await _client.PostTaskAsync(name: "Test2", projectId: 1);

            await _client.PostTaskAsync(name: "Test3", projectId: 1);

            var httpResponce = await _client.GetAsync("api/Project/q1/?id=1");
            var stringPostResponce = await httpResponce.Content.ReadAsStringAsync();
            var projects = JsonConvert.DeserializeObject<List<Query1View>>(stringPostResponce);

            Assert.Single(projects);
            Assert.Equal("Test", projects[0].Project.Name);
            Assert.Equal(3, projects[0].Count);
            Assert.Equal(HttpStatusCode.OK, httpResponce.StatusCode);
        }

        [Fact]
        public async void CreateProject_ThatAlreadyExists_ThenResponceWithCode400()
        {
            await _client.PostProjectAsync("RepeatedProject");

            var httpResponce = await _client.PostProjectAsync("RepeatedProject");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponce.StatusCode);
        }
    }
}
