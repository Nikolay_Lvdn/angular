﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface ITeamService
    {
        Task CreateAsync(TeamDTO teamDTO);
        Task<IList<TeamDTO>> GetAllAsync();
        Task UpdateAsync(TeamDTO teamDTO);
        Task DeleteAsync(int id);
        Task<IList<Query4DTO>> GetQuery4();
    }
}
