﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface IProjectService
    {
        Task CreateAsync(ProjectDTO projectDTO);
        Task<IList<ProjectDTO>> GetAllAsync();
        Task UpdateAsync(ProjectDTO projectDTO);
        Task DeleteAsync(int id);

        Task<IList<Query1DTO>> GetQuery1(int id);
        Task<IList<Query7DTO>> GetQuery7();
    }
}
