﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface ITaskService
    {
        Task CreateAsync(TaskDTO taskDTO);
        Task<IList<TaskDTO>> GetAllAsync();
        Task UpdateAsync(TaskDTO taskDTO);
        Task DeleteAsync(int id);
        Task<IList<TaskDTO>> GetQuery2(int id);
        Task<IList<Query3DTO>> GetQuery3(int id);
        Task<IList<TaskDTO>> GetQuery8(int id);
    }
}
