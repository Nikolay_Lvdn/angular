﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Business.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(TeamDTO teamDTO)
        {
            if (teamDTO == null)
                throw new Exception("Wrong request body");

            if ((await _unitOfWork.GetTeamRepository.GetAsync()).Select(team => team.Name).Contains(teamDTO.Name))
                throw new ArgumentException("Such team is already exists");
            var teamEntity = _mapper.Map<Team>(teamDTO);

            teamEntity.CreatedAt = DateTime.Now;

            await _unitOfWork.GetTeamRepository.CreateAsync(teamEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<TeamDTO>> GetAllAsync()
        {
            return _mapper.Map<IList<Team>, IList<TeamDTO>>(await _unitOfWork.GetTeamRepository.GetAsync());
        }

        public async Task UpdateAsync(TeamDTO teamDTO)
        {
            var teamToUpdate = (await _unitOfWork.GetTeamRepository.GetAsync()).FirstOrDefault(team => team.Id == teamDTO.Id);

            if ((await _unitOfWork.GetTeamRepository.GetAsync()).Select(team => team.Name).Contains(teamDTO.Name))
                throw new ArgumentException("Such team is already exists");

            if (teamToUpdate == null)
                throw new Exception("No such team");

            if ((await _unitOfWork.GetTeamRepository.GetAsync())
                .Where(team => team.Id != teamToUpdate.Id)
                .Select(team => team.Name)
                .Contains(teamDTO.Name))
                throw new ArgumentException("Such team is already exists");

            teamToUpdate.Name = teamDTO.Name;
            await _unitOfWork.GetTeamRepository.UpdateAsync(teamToUpdate);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var teamToDelete = (await _unitOfWork.GetTeamRepository.GetAsync()).FirstOrDefault(team => team.Id == id);

            if (teamToDelete == null)
                throw new Exception("No such team");

            await _unitOfWork.GetTeamRepository.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<Query4DTO>> GetQuery4()
        {
            var a = (await _unitOfWork.GetTeamRepository.GetAsync());
            var teams = (await _unitOfWork.GetTeamRepository.GetAsync())
                .Where(team => team.Members?.Count != 0 && team.Members.All(member => DateTime.Now.Year - member.BirthDay.Year > 10));

            if (!teams.Any())
                throw new ArgumentNullException("There are no such teams");

            return await Task.WhenAll(teams.Select(MakeQuery4Selection));
        }

        private async Task<Query4DTO> MakeQuery4Selection(Team team)
        {
            return await Task.Run(() =>
            {
                return new Query4DTO()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Members = _mapper.Map<IList<User>, IList<UserDTO>>(team.Members.OrderByDescending(member => member.RegisteredAt).ToList())
                };
            });
        }
    }
}
