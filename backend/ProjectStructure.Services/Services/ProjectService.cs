﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Business.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                throw new Exception("Wrong request body");

            if ((await _unitOfWork.GetProjectRepository.GetAsync()).Select(project => project.Name).Contains(projectDTO.Name))
                throw new ArgumentException("Such project is already exists");

            var projectEntity = _mapper.Map<Project>(projectDTO);

            projectEntity.CreatedAt = DateTime.Now;

            await _unitOfWork.GetProjectRepository.CreateAsync(projectEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<ProjectDTO>> GetAllAsync()
        {
            return _mapper.Map<IList<Project>, IList<ProjectDTO>>(await _unitOfWork.GetProjectRepository.GetAsync());
        }

        public async Task UpdateAsync(ProjectDTO projectDTO)
        {
            var projectToUpdate = (await _unitOfWork.GetProjectRepository.GetAsync()).FirstOrDefault(project => project.Id == projectDTO.Id);

            if (projectToUpdate == null)
                throw new Exception("No such project");

            if ((await _unitOfWork.GetProjectRepository.GetAsync())
                .Where(project => project.Id != projectToUpdate.Id)
                .Select(project => project.Name)
                .Contains(projectDTO.Name))
                throw new ArgumentException("Such project is already exists");

            projectToUpdate.AuthorId = projectDTO.AuthorId;
            projectToUpdate.TeamId = projectDTO.TeamId;
            projectToUpdate.Name = projectDTO.Name;
            projectToUpdate.Description = projectDTO.Description;
            projectToUpdate.Deadline = projectDTO.Deadline;
            await _unitOfWork.GetProjectRepository.UpdateAsync(projectToUpdate);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var projectToDelete = (await _unitOfWork.GetProjectRepository.GetAsync()).FirstOrDefault(project => project.Id == id);

            if (projectToDelete == null)
                throw new Exception("No such project");

            await _unitOfWork.GetProjectRepository.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<Query1DTO>> GetQuery1(int id)
        {
            if (!(await _unitOfWork.GetUserRepository.GetAsync()).Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var projects = (await _unitOfWork.GetProjectRepository.GetAsync()).Where(project => project.AuthorId == id);

            if (!projects.Any())
                throw new ArgumentNullException("There are no projects");

            return await Task.WhenAll(projects.Select(MakeQuery1Selection));
        }

        private async Task<Query1DTO> MakeQuery1Selection(Project project)
        {
            return await Task.Run(() =>
            {
                return new Query1DTO()
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    Count = project.Tasks == null ? 0 : project.Tasks.Count
                };
            });
        }

        public async Task<IList<Query7DTO>> GetQuery7()
        {
            var projects = (await _unitOfWork.GetProjectRepository.GetAsync())
                .Where(project => project.Description?.Length > 20 || project.Tasks == null || project.Tasks.Count < 3);

            if (!projects.Any())
                throw new ArgumentNullException("There are no projects");

            return await Task.WhenAll(projects.Select(MakeQuery7Selection));
        }

        private async Task<Query7DTO> MakeQuery7Selection(Project project)
        {
            return await Task.Run(() =>
            {
                return new Query7DTO()
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    LongestTask = _mapper.Map<TaskDTO>(project.Tasks?.OrderBy(task => task.Description.Length).LastOrDefault()),
                    ShortestTask = _mapper.Map<TaskDTO>(project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault()),
                    CountOfMembers = project.Team?.Members?.Count
                };
            });
        }
    }
}
