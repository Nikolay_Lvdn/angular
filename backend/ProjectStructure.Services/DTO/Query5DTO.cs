﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.DTO
{
    public class Query5DTO
    {
        public UserDTO User { get; set; }
        public IList<TaskDTO> Tasks { get; set; }
    }
}
