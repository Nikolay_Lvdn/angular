﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.DTO
{
    public class Query3DTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
