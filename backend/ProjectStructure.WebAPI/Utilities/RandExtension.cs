﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Utilities
{
    public static class RandExtension
    {
        public static DateTime RandomDay(this Random rnd,DateTime from,DateTime to)
        {
            int range = (to - from).Days;
            return from.AddDays(rnd.Next(range));
        }
    }
}
