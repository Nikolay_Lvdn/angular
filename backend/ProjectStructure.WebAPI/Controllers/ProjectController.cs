﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly IMapper _mapper;

        public ProjectController(IProjectService projectService, IMapper mapper)
        {
            _projectService = projectService;
            _mapper = mapper;
        }

        // GET: api/<ProjectController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var projectsDTO = await _projectService.GetAllAsync();
            var projectsView = _mapper.Map<IList<ProjectDTO>, IList<GettingProjectView>>(projectsDTO);

            return Ok(projectsView);
        }

        // GET: api/<TeamController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var projectDTO = (await _projectService.GetAllAsync()).FirstOrDefault(project => project.Id == id);
            if (projectDTO == null)
                return NotFound("No such project");
            var projectView = _mapper.Map<GettingProjectView>(projectDTO);

            return Ok(projectView);
        }

        // POST api/<ProjectController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] ProjectView projectView)
        {
            try
            {
                var projectDTO = _mapper.Map<ProjectView, ProjectDTO>(projectView);

                await _projectService.CreateAsync(projectDTO);

                return CreatedAtAction("POST", projectView);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<ProjectController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] ProjectView projectView)
        {
            try
            {
                var projectDTO = _mapper.Map<ProjectView, ProjectDTO>(projectView);
                projectDTO.Id = id;
                await _projectService.UpdateAsync(projectDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<ProjectController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _projectService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<ProjectController>/q1
        [HttpGet("q1")]
        public async Task<IActionResult> GetQuery1(int id)
        {
            try
            {
                IList<Query1View> result = _mapper.Map<IList<Query1DTO>, IList<Query1View>>(await _projectService.GetQuery1(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<ProjectController>/q2
        [HttpGet("q7")]
        public async Task<IActionResult> GetQuery7()
        {
            try
            {
                IList<Query7View> result = _mapper.Map<IList<Query7DTO>, IList<Query7View>>(await _projectService.GetQuery7());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
