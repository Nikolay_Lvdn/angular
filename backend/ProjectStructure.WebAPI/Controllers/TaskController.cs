﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;

        public TaskController(ITaskService taskService, IMapper mapper)
        {
            _taskService = taskService;
            _mapper = mapper;
        }

        // GET: api/<TaskController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var tasksDTO = await _taskService.GetAllAsync();
            var tasksView = _mapper.Map<IList<TaskDTO>, IList<GettingTaskView>>(tasksDTO);

            return Ok(tasksView);
        }

        // GET: api/<TeamController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var taskDTO = (await _taskService.GetAllAsync()).FirstOrDefault(task => task.Id == id);
            if (taskDTO == null)
                return NotFound("No such task");
            var taskView = _mapper.Map<GettingTaskView>(taskDTO);

            return Ok(taskView);
        }

        // POST api/<TaskController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] TaskView taskView)
        {
            try
            {
                var taskDTO = _mapper.Map<TaskView, TaskDTO>(taskView);

                await _taskService.CreateAsync(taskDTO);

                return CreatedAtAction("POST", taskView);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] TaskView taskView)
        {
            try
            {
                var taskDTO = _mapper.Map<TaskView, TaskDTO>(taskView);
                taskDTO.Id = id;
                await _taskService.UpdateAsync(taskDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _taskService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<TaskController>/q2
        [HttpGet("q2")]
        public async Task<IActionResult> GetQuery2(int id)
        {
            try
            {
                var result = _mapper.Map<IList<TaskDTO>, IList<GettingTaskView>>(await _taskService.GetQuery2(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<TaskController>/q3
        [HttpGet("q3")]
        public async Task<IActionResult> GetQuery3(int id)
        {
            try
            {
                var result = _mapper.Map<IList<Query3DTO>, IList<Query3View>>(await _taskService.GetQuery3(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<TaskController>/q8
        [HttpGet("q8")]
        public async Task<IActionResult> GetQuery8(int id)
        {
            try
            {
                var result = _mapper.Map<IList<TaskDTO>, IList<GettingTaskView>>(await _taskService.GetQuery8(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

    }
}
