﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Business.Interfaces;
using AutoMapper;
using ProjectStructure.Business.DTO;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        // GET: api/<UserController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var membersDTO = await _userService.GetAllAsync();
            var membersView = _mapper.Map<IList<UserDTO>, IList<GettingUserView>>(membersDTO);

            return Ok(membersView);
        }

        // GET: api/<UserController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var userDTO = (await _userService.GetAllAsync()).FirstOrDefault(user => user.Id == id);
            if (userDTO == null)
                return NotFound("No such user");
            var userView = _mapper.Map<GettingUserView>(userDTO);

            return Ok(userView);
        }

        // POST api/<UserController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);

                await _userService.CreateAsync(userDTO);

                return CreatedAtAction("POST", userView);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id,[FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);
                userDTO.Id = id;
                await _userService.UpdateAsync(userDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _userService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<UserController>/q5
        [HttpGet("q5")]
        public async Task<IActionResult> GetQuery5()
        {
            try
            {
                var result = _mapper.Map<IList<Query5DTO>, IList<Query5View>>(await _userService.GetQuery5());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<UserController>/q6
        [HttpGet("q6")]
        public async Task<IActionResult> GetQuery6(int id)
        {
            try
            {
                var result = _mapper.Map<Query6View>(await _userService.GetQuery6(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
