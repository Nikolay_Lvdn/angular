﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query5View
    {
        public UserView User { get; set; }
        public IList<GettingTaskView> Tasks { get; set; }
        public override string ToString()
        {
            string result = $"Name: {User.FirstName} {User.LastName}\n";
            foreach(var task in Tasks)
            {
                result += $"   Task Id: {task.Id} Name: {task.Name}\n";
            }
            return result;
        }
    }
}
