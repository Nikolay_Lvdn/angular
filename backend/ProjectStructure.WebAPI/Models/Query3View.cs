﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query3View
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return $"Task Id: {Id} \nName: {Name} \n";
        }
    }
}
