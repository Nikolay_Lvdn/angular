﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query7View
    {
        public GettingProjectView Project { get; set; }
        public GettingTaskView LongestTask { get; set; }
        public GettingTaskView ShortestTask { get; set; }
        public int? CountOfMembers { get; set; }
        public override string ToString()
        {
            string result = $"Project Id: {Project?.Id} Name: {Project?.Name} \n";
            result+=$"Longest Task\n  Id: {LongestTask?.Id}\n  Name:{LongestTask?.Name}\n  Description:{LongestTask?.Description}\n";
            result+= $"Shortest Task\n  Id: {ShortestTask?.Id}\n  Name:{ShortestTask?.Name}\n";
            return result;
        }
    }
}
