﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class GettingProjectView
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"Project Id: {Id} \nName: {Name} \nAuthorId: {AuthorId}\nTeamId: {TeamId}\n";
        }
    }
}
