﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.WebAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Task, TaskDTO>().ReverseMap();
            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<ProjectDTO, GettingProjectView>().ReverseMap();
            CreateMap<ProjectDTO, ProjectView>().ReverseMap();

            CreateMap<TaskDTO, GettingTaskView>().ReverseMap();
            CreateMap<TaskDTO, TaskView>().ReverseMap();

            CreateMap<TeamDTO, GettingTeamView>().ReverseMap();
            CreateMap<TeamDTO, TeamView>().ReverseMap();

            CreateMap<UserDTO, GettingUserView>().ReverseMap();
            CreateMap<UserDTO, UserView>().ReverseMap();

            CreateMap<Query1DTO, Query1View>()
                .ForMember(view => view.Project, opt => opt.MapFrom(view => view.Project));

            CreateMap<Query3DTO, Query3View>();

            CreateMap<Query4DTO, Query4View>()
                .ForMember(view => view.Members, opt => opt.MapFrom(view => view.Members));

            CreateMap<Query5DTO, Query5View>()
                .ForMember(view => view.User, opt => opt.MapFrom(view => view.User))
                .ForMember(view => view.Tasks, opt => opt.MapFrom(view => view.Tasks));

            CreateMap<Query6DTO, Query6View>()
                .ForMember(view => view.User, opt => opt.MapFrom(view => view.User))
                .ForMember(view => view.LastUserProject, opt => opt.MapFrom(view => view.LastUserProject))
                .ForMember(view => view.LongestTask, opt => opt.MapFrom(view => view.LongestTask));

            CreateMap<Query7DTO, Query7View>()
                .ForMember(view => view.Project, opt => opt.MapFrom(view => view.Project))
                .ForMember(view => view.LongestTask, opt => opt.MapFrom(view => view.LongestTask))
                .ForMember(view => view.ShortestTask, opt => opt.MapFrom(view => view.ShortestTask));

        }
    }
}
