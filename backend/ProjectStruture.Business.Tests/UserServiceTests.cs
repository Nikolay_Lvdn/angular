using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.Business.Services;
using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Repositories;
using ProjectStructure.WebAPI.Mapping;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using FakeItEasy;
using ProjectStructure.DataAccess.Entities;

namespace ProjectStruture.Business.Tests
{
    public class UserServiceTests : IDisposable
    {
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;
        private List<Project> projects = new List<Project>();
        private List<Task> tasks = new List<Task>();
        private List<User> users = new List<User>();

        public UserServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            var _unitOfWork = A.Fake<IUnitOfWork>();
            var _projectRepository = A.Fake<IProjectRepository>();
            var _taskRepository = A.Fake<ITaskRepository>();
            _userRepository = A.Fake<IUserRepository>();

            A.CallTo(() => _unitOfWork.GetProjectRepository).Returns(_projectRepository);
            A.CallTo(() => _unitOfWork.GetTaskRepository).Returns(_taskRepository);
            A.CallTo(() => _unitOfWork.GetUserRepository).Returns(_userRepository);

            A.CallTo(() => _projectRepository.GetAsync()).Returns(projects);
            A.CallTo(() => _taskRepository.GetAsync()).Returns(tasks);
            A.CallTo(() => _userRepository.GetAsync()).Returns(users);

            _userService = new UserService(_unitOfWork, mapper);
        }

        public void Dispose()
        {
            projects.Clear();
            tasks.Clear();
            users.Clear();
        }

        [Fact]
        public async void AddUser_WhenWrongEmail_ThenThrowArgumentException()
        {
            UserDTO userDTO = new UserDTO
            {
                Email = "Wrong"
            };

            await Assert.ThrowsAsync<ArgumentException>(() => _userService.CreateAsync(userDTO));
        }

        [Fact]
        public async void AddUser_WhenUserWithTheSameEmailIsExists_ThenThrowArgumentNullException()
        {
            users.Add(new User { Email = "test@test.test" });

            UserDTO userDTO = new UserDTO
            {
                Email = "test@test.test"
            };

            await Assert.ThrowsAsync<ArgumentException>(() => _userService.CreateAsync(userDTO));
        }

        [Fact]
        public async void AddUser_ThenTrigerGetAndCreateFuncs()
        {
            UserDTO userDTO = new UserDTO();

            await _userService.CreateAsync(userDTO);

            var calls = Fake.GetCalls(_userRepository).ToList();

            Assert.Equal(2, calls.Count);
            Assert.Equal("GetAsync", calls[0].Method.Name);
            Assert.Equal("CreateAsync", calls[1].Method.Name);
        }

        [Fact]
        public async void AddUser_WithTeam_ThenTrigerGetAndCreateFuncs()
        {
            UserDTO userDTO = new UserDTO() { TeamId = 1 };

            await _userService.CreateAsync(userDTO);

            var calls = Fake.GetCalls(_userRepository).ToList();

            Assert.Equal(2, calls.Count);
            Assert.Equal("GetAsync", calls[0].Method.Name);
            Assert.Equal("CreateAsync", calls[1].Method.Name);
        }

        [Fact]
        public async void Query5_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _userService.GetQuery5());
        }

        [Fact]
        public async void Query5_WhenUsersWithTasks_ThenOrdered()
        {
            tasks.Add(new Task { PerformerId = 1, Name = "--" });
            tasks.Add(new Task { PerformerId = 1, Name = "---" });
            tasks.Add(new Task { PerformerId = 2, Name = "--" });
            tasks.Add(new Task { PerformerId = 2, Name = "---" });
            users.Add(new User { Id = 1, FirstName = "B", Tasks = tasks.Where(task => task.PerformerId == 1).ToList() });
            users.Add(new User { Id = 2, FirstName = "A", Tasks = tasks.Where(task => task.PerformerId == 2).ToList() });
            var query5 = await _userService.GetQuery5();

            var query5Expected = query5.OrderBy(q => q.User.FirstName);

            foreach (var q in query5Expected)
            {
                q.Tasks = q.Tasks.OrderByDescending(t => t.Name).ToList();
            }

            Assert.True(query5Expected.SequenceEqual(query5));
        }

        [Fact]
        public async void Query6_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _userService.GetQuery6(1));
        }

        [Fact]
        public async void Query6_WhenUserWithoutTasksAndProjects_ThenSuccess()
        {
            users.Add(new User { Id = 1 });

            await _userService.GetQuery6(1);
        }

        [Fact]
        public async void Query6_WhenExistsUserWithCorrectData_ThenSuccess()
        {
            tasks.Add(new Task { Id = 1, ProjectId = 2, PerformerId = 1, CreatedAt = DateTime.Now });
            tasks.Add(new Task { Id = 2, ProjectId = 2, PerformerId = 1, CreatedAt = DateTime.Now });

            users.Add(new User { Id = 1, Tasks = tasks });

            projects.Add(new Project { Id = 1, AuthorId = 1, CreatedAt = DateTime.Now });
            projects.Add(new Project { Id = 2, AuthorId = 1, CreatedAt = DateTime.Now, Tasks = tasks });

            var query6 = await _userService.GetQuery6(1);

            Assert.Equal(projects[1].Id, query6.LastUserProject.Id);
            Assert.Equal(2, query6.LastProjectTasksCount);
            Assert.Equal(2, query6.CountOfIncompleteOrCanceledTasks);
            Assert.Equal(tasks[0].Id, query6.LongestTask.Id);
        }
    }
}
