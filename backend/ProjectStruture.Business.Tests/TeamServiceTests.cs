﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.Business.Services;
using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Repositories;
using ProjectStructure.WebAPI.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FakeItEasy;
using ProjectStructure.DataAccess.Entities;
using System.Threading;

namespace ProjectStruture.Business.Tests
{
    public class TeamServiceTests : IDisposable
    {
        private readonly ITeamService _teamService;
        private List<User> users = new List<User>();
        private List<Team> teams = new List<Team>();

        public TeamServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });


            IMapper mapper = mappingConfig.CreateMapper();

            var _unitOfWork = A.Fake<IUnitOfWork>();
            var _teamRepository = A.Fake<ITeamRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            A.CallTo(() => _unitOfWork.GetTeamRepository).Returns(_teamRepository);
            A.CallTo(() => _unitOfWork.GetUserRepository).Returns(_userRepository);

            A.CallTo(() => _teamRepository.GetAsync()).Returns(teams);
            A.CallTo(() => _userRepository.GetAsync()).Returns(users);

            _teamService = new TeamService(_unitOfWork, mapper);
        }

        public void Dispose()
        {
            users.Clear();
            teams.Clear();
        }

        [Fact]
        public async void Query4_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _teamService.GetQuery4());
        }

        [Fact]
        public async void Query4_WhenTeamWithoutUsers_ThenArgumentNullException()
        {
            teams.Add(new Team());

            await Assert.ThrowsAsync<ArgumentNullException>(() => _teamService.GetQuery4());
        }

        [Fact]
        public async void Query4_WhenTeamWithUsersThatYungerThen10_ThenArgumentNullException()
        {
            users.Add(new User { TeamId = 1, BirthDay = DateTime.Now });
            users.Add(new User { TeamId = 1, BirthDay = DateTime.Now });
            teams.Add(new Team { Id = 1, Members = users });

            await Assert.ThrowsAsync<ArgumentNullException>(() => _teamService.GetQuery4());
        }

        [Fact]
        public async void Query4_WhenTeamCorrectData_ThenSuccess()
        {
            users.Add(new User { Id = 1, TeamId = 1, BirthDay = new DateTime(2000, 1, 1), RegisteredAt = DateTime.Now });
            users.Add(new User { Id = 2, TeamId = 1, BirthDay = new DateTime(2000, 1, 1), RegisteredAt = DateTime.Now });
            teams.Add(new Team { Id = 1, Members = users });


            var query4 = await _teamService.GetQuery4();

            var query4Expected = query4;
            foreach (var q in query4Expected)

            {
                q.Members = q.Members.OrderByDescending(m => m.RegisteredAt).ToList();
            }

            Assert.True(query4Expected.SequenceEqual(query4));
        }
    }
}
