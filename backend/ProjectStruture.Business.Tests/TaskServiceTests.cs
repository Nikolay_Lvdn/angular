﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.Business.Services;
using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Repositories;
using ProjectStructure.WebAPI.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using FakeItEasy;
using ProjectStructure.DataAccess.Entities;

namespace ProjectStruture.Business.Tests
{
    public class TaskServiceTests : IDisposable
    {
        private readonly ITaskService _taskService;
        private readonly ITaskRepository _taskRepository;
        private List<Task> tasks = new List<Task>();
        private List<User> users = new List<User>();
        public TaskServiceTests()
        {

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            var _unitOfWork = A.Fake<IUnitOfWork>();
            _taskRepository = A.Fake<ITaskRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            A.CallTo(() => _unitOfWork.GetTaskRepository).Returns(_taskRepository);
            A.CallTo(() => _unitOfWork.GetUserRepository).Returns(_userRepository);

            A.CallTo(() => _taskRepository.GetAsync()).Returns(tasks);
            A.CallTo(() => _userRepository.GetAsync()).Returns(users);

            _taskService = new TaskService(_unitOfWork, mapper);
        }

        public void Dispose()
        {
            tasks.Clear();
            users.Clear();
        }

        [Fact]
        public async void Query2_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery2(1));
        }

        [Fact]
        public async void Query2_WhenCorrectTask_ThenSuccess()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { Id = 1, PerformerId = 1, Name = "-" });

            var query2 = await _taskService.GetQuery2(users[0].Id);

            Assert.Single(query2);
            Assert.Equal(tasks[0].Id, query2[0].Id);
        }

        [Fact]
        public async void UpdateTaskState_WhenTaskDoNotExists_ThenArgumentNullException()
        {
            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now };

            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.UpdateAsync(taskDTO));
        }

        [Fact]
        public async void UpdateTaskState_WhenCorrectData_ThenSuccess()
        {
            tasks.Add(new Task { Id = 1, State = 1 });

            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now };

            await _taskService.UpdateAsync(taskDTO);

            var calls = Fake.GetCalls(_taskRepository).ToList();

            Assert.Equal(3, calls.Count);
            Assert.Equal("GetAsync", calls[0].Method.Name);
            Assert.Equal("GetAsync", calls[1].Method.Name);
            Assert.Equal("UpdateAsync", calls[2].Method.Name);
        }

        [Fact]
        public async void UpdateTaskStateAndName_WhenNameIsAlredyExists_ThenArgumentException()
        {
            tasks.Add(new Task { Id = 1, State = 1 });
            tasks.Add(new Task { Id = 2, State = 1, Name = "Task" });

            TaskDTO taskDTO = new TaskDTO { Id = 1, State = 2, FinishedAt = DateTime.Now, Name = "Task" };

            await Assert.ThrowsAsync<ArgumentException>(() => _taskService.UpdateAsync(taskDTO));
        }

        [Fact]
        public async void Query2_When1TaskWithoutName_ThenThrowArgumentNullException()
        {
            tasks.Add(new Task { Id = 1, PerformerId = 1 });

            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery2(1));
        }

        [Fact]
        public async void Query2_When1TaskWithNameLongerThan44_ThenThrowArgumentNullException()
        {
            tasks.Add(new Task { Id = 1, PerformerId = 1, Name = new string('-', 45) });

            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery2(1));
        }

        [Fact]
        public async void Query3_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery3(1));
        }

        [Fact]
        public async void Query3_When1TaskFinished_ThenSuccess()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { Id = 1, PerformerId = 1, FinishedAt = DateTime.Now });

            var query3 = await _taskService.GetQuery3(users[0].Id);

            Assert.Single(query3);
            Assert.Equal(tasks[0].Id, query3[0].Id);
        }

        [Fact]
        public async void Query8_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery8(1));
        }

        [Fact]
        public async void Query8_ThereAre2Uncompletedtasks_ThenThrowArgumentNullException()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 0 });
            tasks.Add(new Task { PerformerId = 1, State = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 2 });
            tasks.Add(new Task { PerformerId = 1, State = 3 });

            var query8 = await _taskService.GetQuery8(users[0].Id);

            Assert.Equal(2, query8.Count);
        }

        [Fact]
        public async void Query8_ThereAreNoUncompletedtasks_ThenThrowArgumentNullException()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { PerformerId = 1, State = 2 });
            tasks.Add(new Task { PerformerId = 1, State = 3 });

            await Assert.ThrowsAsync<ArgumentNullException>(() => _taskService.GetQuery8(users[0].Id));
        }
    }
}
