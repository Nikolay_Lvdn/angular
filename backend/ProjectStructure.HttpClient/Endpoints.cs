﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.WebAPI.Models;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;
using System.Threading;
using System.Diagnostics;

namespace ProjectStructure.ServiceCommunication
{
    public static class Endpoints
    {
        private static HttpClient client = new HttpClient();
        static Endpoints()
        {
            client.BaseAddress = new Uri("https://localhost:44310/");
        }

        #region Logic
        static async Task<IList<T>> GetResponce<T>(string url)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<T>>(await response.Content.ReadAsStringAsync());

        }
        static async Task DeleteResponce(string url)
        {
            HttpResponseMessage response = await client.DeleteAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();
        }
        static async Task PostResponce(string url, string entity)
        {
            HttpResponseMessage response = await client.PostAsync(url, new StringContent(entity, Encoding.UTF8, "application/json"));

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();
        }
        static async Task PutResponce(string url, string entity)
        {
            HttpResponseMessage response = await client.PutAsync(url, new StringContent(entity, Encoding.UTF8, "application/json"));

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();
        }
        public static async Task GetResponceMessage(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.NotFound)
                Console.WriteLine(await response.Content.ReadAsStringAsync());
        }

        #endregion

        #region Get
        public static async Task<IList<Query1View>> GetCountOfTasksOfProjectsByUserId(int id)
        {
            var url = "api/Project/q1/?id=" + id;

            return await GetResponce<Query1View>(url);
        }

        public static async Task<IList<GettingTaskView>> GetTasksByUserIdWithShortName(int id)
        {
            var url = "api/Task/q2/?id=" + id;

            return await GetResponce<GettingTaskView>(url);
        }

        public static async Task<IList<Query3View>> GetFinishedTaskByUserIdInThisYear(int id)
        {
            var url = "api/Task/q3/?id=" + id;

            return await GetResponce<Query3View>(url);
        }

        public static async Task<IList<Query4View>> GetTeamsWithUsersOlderThan9()
        {
            var url = "api/Team/q4";

            return await GetResponce<Query4View>(url);
        }

        public static async Task<IList<Query5View>> GetOrderedUsersAndTasks()
        {
            var url = "api/User/q5";

            return await GetResponce<Query5View>(url);
        }

        public static async Task<Query6View> GetInfoAboutTasksByUserId(int id)
        {
            var url = "api/User/q6/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Query6View>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query7View>> GetInfoAboutProjects()
        {
            var url = "api/Project/q7";

            return await GetResponce<Query7View>(url);
        }
        public static async Task<IList<GettingTaskView>> GetAllUncompletedTasksByUserId(int id)
        {
            var url = "api/Task/q8/?id=" + id;

            return await GetResponce<GettingTaskView>(url);
        }

        public static async Task<IList<GettingProjectView>> GetProjects()
        {
            var url = "api/Project";

            return await GetResponce<GettingProjectView>(url);
        }
        public static async Task<IList<GettingTaskView>> GetTasks()
        {
            var url = "api/Task";

            return await GetResponce<GettingTaskView>(url);
        }
        public static async Task<IList<GettingTeamView>> GetTeams()
        {
            var url = "api/Team";

            return await GetResponce<GettingTeamView>(url);
        }
        public static async Task<IList<GettingUserView>> GetUsers()
        {
            var url = "api/User";

            return await GetResponce<GettingUserView>(url);
        }
        #endregion

        #region Delete
        public static async Task DeleteProject(int id)
        {
            var url = "api/Project/" + id;

            await DeleteResponce(url);
        }
        public static async Task DeleteTask(int id)
        {
            var url = "api/Task/" + id;

            await DeleteResponce(url);
        }
        public static async Task DeleteTeam(int id)
        {
            var url = "api/Team/" + id;

            await DeleteResponce(url);
        }

        public static async Task DeleteUser(int id)
        {
            var url = "api/User/" + id;

            await DeleteResponce(url);
        }
        #endregion

        #region Post
        public static async Task PostProject(ProjectView project)
        {
            var url = "api/Project/";

            await PostResponce(url, JsonConvert.SerializeObject(project));
        }
        public static async Task PostTask(TaskView task)
        {
            var url = "api/Task/";

            await PostResponce(url, JsonConvert.SerializeObject(task));
        }
        public static async Task PostTeam(TeamView team)
        {
            var url = "api/Team/";

            await PostResponce(url, JsonConvert.SerializeObject(team));
        }
        public static async Task PostUser(UserView user)
        {
            var url = "api/User/";

            await PostResponce(url, JsonConvert.SerializeObject(user));
        }
        #endregion

        #region Put

        public static Task<int> MarkRandomTaskWithDelay(int milliseconds)
        {
            var tcs = new TaskCompletionSource<int>();
            System.Timers.Timer timer = new System.Timers.Timer();

            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    var url = "api/Task";

                    var tasks = await GetResponce<GettingTaskView>(url);

                    var randTaskIndex = new Random().Next(tasks.Count());

                    var taskToUpdate = tasks[randTaskIndex];

                    if (taskToUpdate.State == 2)
                        throw new ArgumentException("Task is already done");

                    if (taskToUpdate.State == 3)
                        throw new ArgumentException("Task is cancelled");

                    taskToUpdate.State = 2;
                    taskToUpdate.FinishedAt = DateTime.Now;

                    var putUrl = "api/Task/" + tasks[randTaskIndex].Id;

                    await PutResponce(putUrl, JsonConvert.SerializeObject(taskToUpdate));
                    
                    tcs.TrySetResult(tasks[randTaskIndex].Id);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };

            timer.Interval = milliseconds;
            timer.AutoReset = false;
            timer.Start();

            return tcs.Task;
        }

        public static async Task PutProject(int id, ProjectView project)
        {
            var url = "api/Project/" + id;

            await PutResponce(url, JsonConvert.SerializeObject(project));
        }
        public static async Task PutTask(int id, TaskView task)
        {
            var url = "api/Task/" + id;

            await PutResponce(url, JsonConvert.SerializeObject(task));
        }
        public static async Task PutTeam(int id, TeamView team)
        {
            var url = "api/Team/" + id;

            await PutResponce(url, JsonConvert.SerializeObject(team));
        }
        public static async Task PutUser(int id, UserView user)
        {
            var url = "api/User/" + id;

            await PutResponce(url, JsonConvert.SerializeObject(user));
        }
        #endregion
    }
}
