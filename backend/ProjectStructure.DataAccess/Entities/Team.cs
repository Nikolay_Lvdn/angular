﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Entities
{
    public class Team : BaseEntity
    {
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<User> Members { get; set; }
    }
}
