﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Entities
{
    public class Project : BaseEntity
    {
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [StringLength(150, MinimumLength = 3)]
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

    }
}
