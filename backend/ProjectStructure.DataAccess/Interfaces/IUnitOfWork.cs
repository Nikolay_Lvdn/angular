﻿using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        public IProjectRepository GetProjectRepository { get; }
        public ITaskRepository GetTaskRepository { get; }
        public ITeamRepository GetTeamRepository { get; }
        public IUserRepository GetUserRepository { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
