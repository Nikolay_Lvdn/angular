﻿using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
