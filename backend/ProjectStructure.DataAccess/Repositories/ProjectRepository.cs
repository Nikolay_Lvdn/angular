﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(CompanyDbContext context)
            : base(context)
        {
        }
        public override async Task<IList<Project>> GetAsync()
        {
            return await _context.Projects
                .Include(project => project.Tasks)
                .Include(project => project.Team)
                .Include(project => project.Team.Members)
                .ToListAsync();
        }
    }
}
