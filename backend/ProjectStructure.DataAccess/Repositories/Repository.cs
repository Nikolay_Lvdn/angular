﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly CompanyDbContext _context;

        public Repository(CompanyDbContext context)
        {
            _context = context;
        }

        public virtual async Task CreateAsync(TEntity entity) 
        {
             await _context.Set<TEntity>().AddAsync(entity);
        }

        public virtual async Task DeleteAsync(int id) {
            TEntity entity = await _context.Set<TEntity>().FindAsync(id);
            await DeleteAsync(entity);
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            var dbSet = _context.Set<TEntity>();

            if (_context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }

            await Task.Run(() => dbSet.Remove(entity));
        }

        public virtual async Task<IList<TEntity>> GetAsync()
        {
            IQueryable<TEntity> query = _context.Set<TEntity>().IgnoreAutoIncludes();

            return await query.ToListAsync();
        }

        public virtual async Task UpdateAsync(TEntity entity)
        {
            await Task.Run(() => _context.Update(entity));
        }
    }
}
