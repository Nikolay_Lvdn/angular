﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure.DataAccess.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(CompanyDbContext context)
            : base(context)
        {
        }
        public override async Task<IList<Team>> GetAsync()
        {
            return await _context.Teams
                .Include(team => team.Members)
                .ToListAsync();
        }
    }
}
