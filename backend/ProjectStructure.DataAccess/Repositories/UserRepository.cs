﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CompanyDbContext context)
            : base(context)
        {
        }
        public override async Task<IList<User>> GetAsync()
        {
            return await _context.Users
                .Include(user => user.Tasks)
                .ToListAsync();
        }
    }
}
