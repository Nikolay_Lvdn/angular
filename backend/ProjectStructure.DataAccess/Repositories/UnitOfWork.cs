﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CompanyDbContext _context;
        private IProjectRepository _projectRepository;
        private ITaskRepository _taskRepository;
        private ITeamRepository _teamRepository;
        private IUserRepository _userRepository;

        public UnitOfWork(CompanyDbContext context)
        {
            _context = context;
        }

        public IProjectRepository GetProjectRepository
        {
            get
            {
                return _projectRepository = _projectRepository
                    ?? new ProjectRepository(_context);
            }
        }

        public ITaskRepository GetTaskRepository
        {
            get
            {
                return _taskRepository = _taskRepository
                    ?? new TaskRepository(_context);
            }
        }

        public ITeamRepository GetTeamRepository
        {
            get
            {
                return _teamRepository = _teamRepository
                    ?? new TeamRepository(_context);
            }
        }

        public IUserRepository GetUserRepository
        {
            get
            {
                return _userRepository = _userRepository
                    ?? new UserRepository(_context);
            }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }


    }
}
