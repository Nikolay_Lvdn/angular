export class Task {
    id: number;
    projectId: number;
    performerId: number;
    name: string;
    description: string;
    state: number;
    createdAt: Date;
    finishedAt?: Date;
}