import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { ProjectListComponent } from './components/project/project-list/project-list.component';
import { TaskListComponent } from './components/task/task-list/task-list.component';
import { TeamListComponent } from './components/team/team-list/team-list.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { TeamEditComponent } from './components/team/team-edit/team-edit.component';
import { TeamAddComponent } from './components/team/team-add/team-add.component';
import { DatePipe } from './pipes/date.pipe';
import { UserEditComponent } from './components/user/user-edit/user-edit.component';
import { UserAddComponent } from './components/user/user-add/user-add.component';
import { ProjectEditComponent } from './components/project/project-edit/project-edit.component';
import { ProjectAddComponent } from './components/project/project-add/project-add.component';
import { TaskEditComponent } from './components/task/task-edit/task-edit.component';
import { TaskAddComponent } from './components/task/task-add/task-add.component';
import { TaskMarkDirective } from './directives/task-mark.directive';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ProjectListComponent,
    TaskListComponent,
    TeamListComponent,
    UserListComponent,
    TeamEditComponent,
    TeamAddComponent,
    DatePipe,
    UserEditComponent,
    UserAddComponent,
    ProjectEditComponent,
    ProjectAddComponent,
    TaskEditComponent,
    TaskAddComponent,
    TaskMarkDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      progressBar: true,
      progressAnimation: "increasing"
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
