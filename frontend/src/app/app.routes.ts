import { Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { ProjectListComponent } from './components/project/project-list/project-list.component';
import { TaskListComponent } from './components/task/task-list/task-list.component';
import { TeamListComponent } from './components/team/team-list/team-list.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { TeamEditComponent } from './components/team/team-edit/team-edit.component';
import { DeactivateGuard } from './guards/deactivate.guard';
import { TeamAddComponent } from './components/team/team-add/team-add.component';
import { UserEditComponent } from './components/user/user-edit/user-edit.component';
import { UserAddComponent } from './components/user/user-add/user-add.component';
import { ProjectEditComponent } from './components/project/project-edit/project-edit.component';
import { ProjectAddComponent } from './components/project/project-add/project-add.component';
import { TaskEditComponent } from './components/task/task-edit/task-edit.component';
import { TaskAddComponent } from './components/task/task-add/task-add.component';

export const AppRoutes: Routes = [
    { path: '', component: MainComponent, pathMatch: 'full' },
    { path: 'project', component: ProjectListComponent, pathMatch: 'full' },
    { path: 'project/add', component: ProjectAddComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'project/:id', component: ProjectEditComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'task', component: TaskListComponent, pathMatch: 'full' },
    { path: 'task/add', component: TaskAddComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'task/:id', component: TaskEditComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'team', component: TeamListComponent, pathMatch: 'full' },
    { path: 'team/add', component: TeamAddComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'team/:id', component: TeamEditComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'user', component: UserListComponent, pathMatch: 'full' },
    { path: 'user/add', component: UserAddComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: 'user/:id', component: UserEditComponent, pathMatch: 'full', canDeactivate: [DeactivateGuard] },
    { path: '**', redirectTo: '' }
];
