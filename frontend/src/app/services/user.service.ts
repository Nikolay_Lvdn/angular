import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public routePrefix = '/api/user';

  constructor(private httpService: HttpInternalService) { }

  public getUsers() {
    return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }

  public getUser(id: number) {
    return this.httpService.getFullRequest<User>(`${this.routePrefix}/${id}`);
  }

  public createUser(post: User) {
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`, post);
  }

  public deleteUser(user: User) {
    return this.httpService.deleteFullRequest<User>(`${this.routePrefix}/${user.id}`);
  }

  public updateUser(user: User) {
    return this.httpService.putFullRequest<User>(`${this.routePrefix}/${user.id}`, user);
  }
}
