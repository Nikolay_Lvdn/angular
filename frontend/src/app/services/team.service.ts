import { Injectable } from '@angular/core';
import { Team } from '../models/team';
import { HttpInternalService } from './http-internal.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public routePrefix = '/api/team';

  constructor(private httpService: HttpInternalService) { }

  public getTeams() {
    return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  public getTeam(id: number) {
    return this.httpService.getFullRequest<Team>(`${this.routePrefix}/${id}`);
  }

  public createTeam(post: Team) {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, post);
  }

  public deleteTeam(team: Team) {
    return this.httpService.deleteFullRequest<Team>(`${this.routePrefix}/${team.id}`);
  }

  public updateTeam(team: Team) {
    return this.httpService.putFullRequest<Team>(`${this.routePrefix}/${team.id}`, team);
  }
}
