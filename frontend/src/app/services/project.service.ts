import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public routePrefix = '/api/project';

  constructor(private httpService: HttpInternalService) { }

  public getProjects() {
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  public getProject(id: number) {
    return this.httpService.getFullRequest<Project>(`${this.routePrefix}/${id}`);
  }

  public createProject(post: Project) {
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, post);
  }

  public deleteProject(project: Project) {
    return this.httpService.deleteFullRequest<Project>(`${this.routePrefix}/${project.id}`);
  }

  public updateProject(project: Project) {
    return this.httpService.putFullRequest<Project>(`${this.routePrefix}/${project.id}`, project);
  }
}
