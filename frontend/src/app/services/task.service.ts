import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Task } from 'src/app/models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public routePrefix = '/api/task';

  constructor(private httpService: HttpInternalService) { }

  public getTasks() {
    return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
  }

  public getTask(id: number) {
    return this.httpService.getFullRequest<Task>(`${this.routePrefix}/${id}`);
  }

  public createTask(post: Task) {
    return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, post);
  }

  public deleteTask(task: Task) {
    return this.httpService.deleteFullRequest<Task>(`${this.routePrefix}/${task.id}`);
  }

  public updateTask(task: Task) {
    if(task.finishedAt==null || task.finishedAt.toString()=="") task.finishedAt=null;
    return this.httpService.putFullRequest<Task>(`${this.routePrefix}/${task.id}`, task);
  }
}
