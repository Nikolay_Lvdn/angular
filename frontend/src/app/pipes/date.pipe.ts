import { Pipe, PipeTransform } from '@angular/core';


const monthNames = ["Січня", "Лютого", "Березня", "Квітня", "Травня", "Червня",
  "Липня", "Серпня", "Вересень", "Жовтня", "Листопада", "Груденя"
];

@Pipe({
  name: 'myDate'
})
export class DatePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): string {
    if(value==null)
      return 'Not finished yet';

    let date=new Date(value);
    return `${date.getDate()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`;
  }

}
