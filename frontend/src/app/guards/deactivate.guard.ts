import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TeamAddComponent } from '../components/team/team-add/team-add.component';
import { TeamEditComponent } from '../components/team/team-edit/team-edit.component';
import { UserEditComponent } from '../components/user/user-edit/user-edit.component';
import { UserAddComponent } from '../components/user/user-add/user-add.component';
import { ProjectEditComponent } from '../components/project/project-edit/project-edit.component';
import { ProjectAddComponent } from '../components/project/project-add/project-add.component';
import { TaskEditComponent } from '../components/task/task-edit/task-edit.component';
import { TaskAddComponent } from '../components/task/task-add/task-add.component';

@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate<TeamEditComponent | TeamAddComponent | UserEditComponent | UserAddComponent 
| ProjectEditComponent | ProjectAddComponent | TaskEditComponent | TaskAddComponent> {
  canDeactivate(
    component: TeamEditComponent | TeamAddComponent | UserEditComponent | UserAddComponent 
    | ProjectEditComponent | ProjectAddComponent | TaskEditComponent | TaskAddComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return !component.hasUnsavedData() || confirm("Make sure you are saved everything. Continue?");
  }
  
}
