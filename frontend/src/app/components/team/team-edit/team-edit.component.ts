import { Component, OnInit, ViewChild } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css']
})
export class TeamEditComponent implements OnInit {

  public team: Team = new Team();
  public formSubmited: boolean;

  @ViewChild('teamForm')
  public teamForm: NgForm;

  constructor(
    private teamService: TeamService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;

    let id = this.route.snapshot.paramMap.get('id');
    this.teamService.getTeam(+id)
      .subscribe(resp => {
        this.team = resp.body;
      },
      (error) => (this.toastr.error(error.error,'Error')))
  }

  public hasUnsavedData() : boolean {
    return this.teamForm.dirty;
  }
  
  public onSubmited()
  {
    this.teamService.updateTeam(this.team)
      .subscribe((resp) => {
        this.toastr.success('Updated')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
