import { Component, OnInit, ViewChild } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-team-add',
  templateUrl: './team-add.component.html',
  styleUrls: ['./team-add.component.css']
})
export class TeamAddComponent implements OnInit {

  public team: Team = new Team();
  public formSubmited: boolean;

  @ViewChild('teamForm')
  public teamForm: NgForm;

  constructor(
    private teamService: TeamService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;
  }

  public hasUnsavedData() : boolean {
    return this.teamForm.dirty;
  }
  
  public onSubmited()
  {
    this.teamService.createTeam(this.team)
      .subscribe(resp => {
        this.toastr.success('Created')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
