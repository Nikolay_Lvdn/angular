import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { TeamService } from 'src/app/services/team.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

  private unsubscribe$ = new Subject<void>();
  teams: Team[] = [];
  team: Team;

  constructor(
    private teamService: TeamService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getTeams();
  }

  public getTeams() {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
  }

  deleteTeam(team: Team) {
    if (confirm('Are you sure and you still want to delete this team?')) {
      this.teamService.deleteTeam(team).subscribe(
        (resp) => {
          this.toastr.success('Deleted');
          this.getTeams();
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
    }
  }

}
