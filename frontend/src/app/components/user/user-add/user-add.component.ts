import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  public user: User = new User();
  public formSubmited: boolean;

  @ViewChild('userForm')
  public userForm: NgForm;

  constructor(
    private userService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;
  }

  public hasUnsavedData() : boolean {
    return this.userForm.dirty;
  }
  
  public onSubmited()
  {
    
    this.userService.createUser(this.user)
      .subscribe(resp => {
        this.toastr.success('Created')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }
}
