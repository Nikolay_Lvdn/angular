import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Observable, Subject } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  private unsubscribe$ = new Subject<void>();
  users: User[] = [];
  user: User;

  constructor(
    private userService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers() {
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.users = resp.body;
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
  }

  deleteUser(user: User) {
    if (confirm('Are you sure and you still want to delete this user?')) {
      this.userService.deleteUser(user).subscribe(
        (resp) => {
          this.toastr.success('Deleted');
          this.getUsers();
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
    }
  }

}
