import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  public user: User = new User();
  public formSubmited: boolean;

  @ViewChild('userForm')
  public userForm: NgForm;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;

    let id = this.route.snapshot.paramMap.get('id');
    this.userService.getUser(+id)
      .subscribe(resp => {
        this.user = resp.body;
    },
    (error) => (this.toastr.error(error.error,'Error')))
  }

  public hasUnsavedData() : boolean {
    return this.userForm.dirty;
  }
  
  public onSubmited()
  {
    
    this.userService.updateUser(this.user)
      .subscribe(resp => {
        this.toastr.success('Updated')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
