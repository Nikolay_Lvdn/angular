import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  public project: Project = new Project();
  public formSubmited: boolean;

  @ViewChild('projectForm')
  public projectForm: NgForm;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;

    let id = this.route.snapshot.paramMap.get('id');
    this.projectService.getProject(+id)
      .subscribe(resp => {
        this.project = resp.body;
    },
    (error) => (this.toastr.error(error.error,'Error')))
  }

  public hasUnsavedData() : boolean {
    return this.projectForm.dirty;
  }
  
  public onSubmited()
  {
    
    this.projectService.updateProject(this.project)
      .subscribe(resp => {
        this.toastr.success('Updated')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
