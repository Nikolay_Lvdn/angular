import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { ProjectService } from 'src/app/services/project.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  private unsubscribe$ = new Subject<void>();
  projects: Project[] = [];
  project: Project;

  constructor(
    private projectService: ProjectService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getProjects();
  }

  public getProjects() {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body;
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
  }

  deleteProject(project: Project) {
    if (confirm('Are you sure and you still want to delete this user?')) {
      this.projectService.deleteProject(project).subscribe(
        (resp) => {
          this.toastr.success('Deleted');
          this.getProjects();
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
    }
  }
}
