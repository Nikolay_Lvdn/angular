import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {

  public project: Project = new Project();
  public formSubmited: boolean;

  @ViewChild('projectForm')
  public projectForm: NgForm;

  constructor(
    private projectService: ProjectService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;
  }

  public hasUnsavedData() : boolean {
    return this.projectForm.dirty;
  }
  
  public onSubmited()
  {
    
    this.projectService.createProject(this.project)
      .subscribe(resp => {
        this.toastr.success('Created')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
