import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  public task: Task = new Task();
  public formSubmited: boolean;

  @ViewChild('taskForm')
  public taskForm: NgForm;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;

    let id = this.route.snapshot.paramMap.get('id');
    this.taskService.getTask(+id)
      .subscribe(resp => {
        this.task = resp.body;
    },
    (error) => (this.toastr.error(error.error,'Error')))
  }

  public hasUnsavedData() : boolean {
    return this.taskForm.dirty;
  }
  
  public onSubmited()
  {
    
    this.taskService.updateTask(this.task)
      .subscribe((resp) => {
        this.toastr.success('Updated')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }

}
