import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {

  public task: Task = new Task();
  public formSubmited: boolean;

  @ViewChild('taskForm')
  public taskForm: NgForm;

  constructor(
    private taskService: TaskService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.formSubmited = false;
  }

  public hasUnsavedData() : boolean {
    return this.taskForm.dirty;
  }
  
  public onSubmited()
  {
    this.taskService.createTask(this.task)
      .subscribe(resp => {
        this.toastr.success('Created')
      },
      (error) => (this.toastr.error(error.error,'Error')))
    this.formSubmited = true;
  }
}
