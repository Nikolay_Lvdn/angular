import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { Observable, Subject } from 'rxjs';
import { TaskService } from 'src/app/services/task.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  private unsubscribe$ = new Subject<void>();
  tasks: Task[] = [];
  task: Task;

  constructor(
    private taskService: TaskService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getTasks();
  }

  public getTasks() {
    this.taskService
      .getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.tasks = resp.body;
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
  }

  deleteTask(task: Task) {
    if (confirm('Are you sure and you still want to delete this user?')) {
      this.taskService.deleteTask(task).subscribe(
        (resp) => {
          this.toastr.success('Deleted');
          this.getTasks();
        },
        (error) => (this.toastr.error(error.error,'Error'))
      );
    }
  }
}
