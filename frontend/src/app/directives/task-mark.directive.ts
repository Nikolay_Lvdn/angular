import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appTaskMark]'
})
export class TaskMarkDirective implements OnInit {

  @Input() state: number;

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    switch (this.state) {
      case 0:
        this.el.nativeElement.innerHTML = this.getTemplate('#C1C1C1','To Do');
        break;
      case 1:
        this.el.nativeElement.innerHTML = this.getTemplate('#1E90FF','In Progress');
        break;
      case 2:
        this.el.nativeElement.innerHTML = this.getTemplate('#32CD32','Done');
        break;
      case 3:
        this.el.nativeElement.innerHTML = this.getTemplate('#F54748','Canceled');
        break;
    }
  }

  public getTemplate(color: string, info: string): string {
    return `<i class=\"fas fa-circle\" style=\"color: ${color};\"></i><span>${info}</span>`;
  }

}
